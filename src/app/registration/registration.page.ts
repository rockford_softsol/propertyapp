import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  name: any
  mobile: any
  email: any
  address: any
  password: any
  repassword: any
  buttonStatus: any

  constructor(public toastController: ToastController, private http: HttpClient, private router: Router) { 
    this.name=''
    this.mobile=''
    this.email=''
    this.address=''
    this.password =''
    this.repassword =''

    this.buttonStatus = false
  }

  ngOnInit() {

  }

  checkEmail(){
    var formData = {
      email: this.email
    };
    this.http.post('http://localhost:3000/checkEmailAvailability', formData).subscribe(async (response) => {
        console.log(JSON.parse(JSON.stringify(response)));
        if(JSON.parse(JSON.stringify(response)).length > 0){
          const toast = await this.toastController.create({
            message: '<center>Email ID already exists</center>',
            duration: 2000,
            color: 'info',
            position: 'bottom',
            showCloseButton: true
          });
          toast.present();
          this.buttonStatus = true;
      }
      else{
        this.buttonStatus = false;
      }     
    });
  }

  async saveUserDetails(){
    if(this.name == '' || this.mobile == '' || this.email == '' || this.address == '' || this.mobile.length < 10 || this.mobile.length > 10 || this.password == ''){
      const toast = await this.toastController.create({
        message: '<center>Please fill all details</center>',
        duration: 2000,
        color: 'danger',
        position: 'bottom',
        showCloseButton: true
      });
      toast.present();
    }
    else{   
      
      if(this.password == this.repassword){
        var formData = {
          name: this.name,
          mobile: this.mobile,
          address: this.address,
          email: this.email,
          password: this.password
        };
        this.http.post('http://localhost:3000/saveUser', formData).subscribe(async (response) => {
          if (JSON.parse(JSON.stringify(response)).status == 'success') {  
            this.name=''
            this.mobile=''
            this.email=''
            this.address=''
            this.password=''
            this.repassword=''
            const toast = await this.toastController.create({
              message: '<center>Signup Success</center>',
              duration: 2000,
              color: 'success',
              position: 'top',
              showCloseButton: true
            });
            toast.present();
            this.router.navigateByUrl('login');
          }
          if (JSON.parse(JSON.stringify(response)).status == 'failed') {
            const toast = await this.toastController.create({
              message: '<center>Please Try Again...</center>',
              duration: 2000,
              color: 'danger',
              position: 'top',
              showCloseButton: true
            });
            toast.present();
          }
        });
      }
      else{
        const toast = await this.toastController.create({
          message: '<center>Passwords Do Not Match...</center>',
          duration: 2000,
          color: 'danger',
          position: 'top',
          showCloseButton: true
        });
        toast.present();
      }


    }
  }

}
