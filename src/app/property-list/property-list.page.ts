import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';

import { FilterPipe } from 'ngx-filter-pipe';

@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.page.html',
  styleUrls: ['./property-list.page.scss'],
})
export class PropertyListPage implements OnInit {
  
  constructor(
    private router: Router,
    private http: HttpClient,
    public toastController: ToastController,
    private filter: FilterPipe
  ) { }

  properties_list: any;
  propertyFilter: any = { address: '' };

  async ngOnInit() {
    if(localStorage.getItem('userEmail') == null || localStorage.getItem('userEmail') == '' || localStorage.getItem('userEmail') == undefined){
      const toast = await this.toastController.create({
        message: '<center>You are not logged in</center>',
        duration: 2000,
        color: 'danger',
        position: 'bottom',
        showCloseButton: true
      });
      toast.present();
      this.router.navigateByUrl('login');
    }
    this.getAllProperty();
  }

  getAllProperty() {
    this.http.get('http://localhost:3000/getAllProperty').subscribe((response) => {
      this.properties_list = JSON.parse(JSON.stringify(response))         
    });  
  }
}
