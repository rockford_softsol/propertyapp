import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  userInfo = []

  name: any
  mobile: any
  email: any
  address: any

  constructor(
    private router: Router,
    public toastController: ToastController,
    private http: HttpClient
  ) { }

  async ngOnInit() {
    if(localStorage.getItem('userEmail') == null || localStorage.getItem('userEmail') == '' || localStorage.getItem('userEmail') == undefined){
      const toast = await this.toastController.create({
        message: '<center>You are not logged in</center>',
        duration: 2000,
        color: 'danger',
        position: 'bottom',
        showCloseButton: true
      });
      toast.present();
      this.router.navigateByUrl('login');
    }

    this.getUserInfo();
  }


  getUserInfo(){
    var formData = {
      email: localStorage.getItem('userEmail')
    };
    this.http.post('http://localhost:3000/getUserInfo', formData).subscribe(async (response) => {
        this.userInfo = JSON.parse(JSON.stringify(response));
        this.name = this.userInfo[0].name;
        this.mobile = this.userInfo[0].mobile;
        this.email = this.userInfo[0].email;
        this.address = this.userInfo[0].address;
    });
  }

  logout(){
    localStorage.removeItem('userEmail');
    this.router.navigateByUrl('login');
  }

}
