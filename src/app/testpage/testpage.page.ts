import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-testpage',
  templateUrl: './testpage.page.html',
  styleUrls: ['./testpage.page.scss'],
})
export class TestpagePage implements OnInit {
  firstName: any
  lastName: any

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  submitValues(){
    this.http.get('http://localhost:3000/saveName?firstname='+this.firstName+'&lastname='+this.lastName+'').subscribe((response) => {
      if(JSON.parse(JSON.stringify(response)).status == 'success'){
        this.firstName = '';
        this.lastName = '';
        alert('Successfully Inserted to DB');        
      }
      if(JSON.parse(JSON.stringify(response)).status == 'failed'){        
        alert('Please try again');
      }
    });   
  }

}
