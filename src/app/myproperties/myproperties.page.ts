import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-myproperties',
  templateUrl: './myproperties.page.html',
  styleUrls: ['./myproperties.page.scss'],
})
export class MypropertiesPage implements OnInit {

  mypropertylist: any
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getMyProperty()
  }

  getMyProperty() {
    this.http.post('http://localhost:3000/getMyProperties', {email: localStorage.getItem('userEmail')}).subscribe((response) => {
      this.mypropertylist = response;
    });  
  }

  deleteProperty(property){
    this.http.post('http://localhost:3000/deleteMyProperties', property).subscribe((response) => {
      this.mypropertylist = []
      this.getMyProperty();
    });
  }

}
