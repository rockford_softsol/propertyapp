import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MypropertiesPage } from './myproperties.page';

describe('MypropertiesPage', () => {
  let component: MypropertiesPage;
  let fixture: ComponentFixture<MypropertiesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MypropertiesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MypropertiesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
