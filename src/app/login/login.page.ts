import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: any
  password: any
  errorMessage = []

  constructor(private router: Router, public toastController: ToastController, private http: HttpClient) {
    this.email = ''
    this.password = ''
    this.errorMessage = []
  }

  ngOnInit() {
  }

  async checkUser() {    
    if (this.email == '') {
     this.errorMessage.push('* Please enter email');
    }
    if (this.password == '') {
      this.errorMessage.push('* Please enter password');
    }    
    if(this.email != '' || this.password != ''){

      var formData = {
        email: this.email,
        password: this.password
      }
      this.http.post('http://localhost:3000/checkUserLogin', formData).subscribe(async (response) => {
          if(JSON.parse(JSON.stringify(response)).length > 0){
              localStorage.setItem('userEmail',response[0].email);
              this.router.navigateByUrl('home');
          }
          else{
            const toast = await this.toastController.create({
              message: '<center>INCORRECT USERNAME / PASSWORD</center>',
              duration: 2000,
              color: 'danger',
              position: 'bottom',
              showCloseButton: true
            });
            toast.present();
          }
        });
    }
  }

  goToRegistration() {
    this.router.navigateByUrl('/registration');
  }

}
