import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  slider_property_list: any
  // slideOptsOne = {initialSlide: 0,slidesPerView: 1.2,autoplay:true, spaceBetween: 10};

  sliderOption = {
    initialSlide: 0,
    autoplay: true,
    speed: 600
  };

  constructor(
    private router: Router,
    private http: HttpClient,
    public toastController: ToastController
  ) { }

  async ngOnInit(){
    if(localStorage.getItem('userEmail') == null || localStorage.getItem('userEmail') == '' || localStorage.getItem('userEmail') == undefined){
      const toast = await this.toastController.create({
        message: '<center>You are not logged in</center>',
        duration: 2000,
        color: 'danger',
        position: 'bottom',
        showCloseButton: true
      });
      toast.present();
      this.router.navigateByUrl('login');
    }
    this.getAllProperty();
  }

  goToTestPage() {
    this.router.navigateByUrl('testpage');
  }

  getAllProperty() {
    this.http.get('http://localhost:3000/getAllProperty').subscribe((response) => {
      this.slider_property_list = response;      
    });
  }

}
