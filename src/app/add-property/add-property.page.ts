import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-property',
  templateUrl: './add-property.page.html',
  styleUrls: ['./add-property.page.scss'],
})
export class AddPropertyPage implements OnInit {

  address: any
  price: any
  propertyType: any
  description: any
  numberOfBeds: any
  numberOfBaths: any
  area: any
  imageFile: any

  constructor(private http: HttpClient, public toastController: ToastController, private router: Router) { }

  async ngOnInit() {
    if (localStorage.getItem('userEmail') == null || localStorage.getItem('userEmail') == '' || localStorage.getItem('userEmail') == undefined) {
      const toast = await this.toastController.create({
        message: '<center>You are not logged in</center>',
        duration: 2000,
        color: 'danger',
        position: 'bottom',
        showCloseButton: true
      });
      toast.present();
      this.router.navigateByUrl('login');
    }


    this.address = '';
    this.price = '';
    this.propertyType = '';
    this.description = '';
    this.numberOfBeds = '';
    this.numberOfBaths = '';
    this.area = '';
    this.imageFile = '';
  }

  async save() {
    if (this.imageFile == '' || this.address == '' || this.price == '' || this.propertyType == '' || this.numberOfBeds == '' || this.numberOfBaths == '' || this.area == '' || this.description == '') {
      const toast = await this.toastController.create({
        message: '<center>Please fill all details</center>',
        duration: 2000,
        color: 'danger',
        position: 'top',
        showCloseButton: true
      });
      toast.present();
    }
    else {

      var formData = {
        image: this.imageFile,
        address: this.address,
        price: this.price,
        propertyType: this.propertyType,
        description: this.description,
        number_of_beds: this.numberOfBeds,
        number_of_baths: this.numberOfBaths,
        area: this.area,
        user: localStorage.getItem('userEmail')
      };

      this.http.post('http://localhost:3000/saveProperty', formData).subscribe(async (response) => {
        if (JSON.parse(JSON.stringify(response)).status == 'success') {
          this.address = '';
          this.price = '';
          this.propertyType = '';
          this.description = '';
          this.numberOfBeds = '';
          this.numberOfBaths = '';
          this.area = '';
          this.imageFile = '';
          const toast = await this.toastController.create({
            message: '<center>Successfully Inserted to DB</center>',
            duration: 2000,
            color: 'success',
            position: 'top',
            showCloseButton: true
          });
          toast.present();
        }
        if (JSON.parse(JSON.stringify(response)).status == 'failed') {
          const toast = await this.toastController.create({
            message: '<center>Please Try Again...</center>',
            duration: 2000,
            color: 'danger',
            position: 'top',
            showCloseButton: true
          });
          toast.present();
        }
      });
    }
  }

  fileChanged(fileData) {
    var file = fileData.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this.fileReaderReady.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  fileReaderReady(fileReaderObject) {
    var binaryString = fileReaderObject.target.result;
    this.imageFile = btoa(binaryString);
  }

}
